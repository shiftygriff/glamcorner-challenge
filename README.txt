Sam Griffin Programming Challenge- Connect 4

This repository includes both the source code and a compiled jar file.

Explanation:
The game is built with a model view controller pattern. Implementations of the abstract Player class serve as the views. The game can easily be extended with new types of players by creating a new implementation of the Player class and implementing it's run() method.
The Model class implements Java's built-in Observable interface, which allows it to notify it's Observers (the Players) whenever it has changed so that they can retrieve the latest state.
Threading allows Player objects to run in parallel, making it possible to implement a version with another human player in a seperate window or over a network that can respond to user input independently. This is simulated with a short artificial delay in the computer player's responses.
The use of seperate packages allows us to take advantage of Java's privacy controls to prevent Player objects from modifying the Model directly and forcing them to use the methods exposed by the Controller. The Controller also implements an authentication token mechanism that prevents Player objects from making moves on behalf of their opponent. 
There are no dependencies outside of Java's core libraries and JUnit for running unit tests.

To launch the executable, run "java -jar Connect4.jar".

The entry point for the program in the source code is the core.GameManager class.
JUnit4 tests for the core package are provided in core.CoreTests.