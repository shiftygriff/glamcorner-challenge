package core;

import player.Player;

/**
 * This class handles the logic of passing Player commands to the Model.
 *
 */
public class Controller {
	
	private Player player1;
	private Player player2;
	
	//these are unique tokens used to identify which Player requests come from
	private String player1Token;
	private String player2Token;

	private Model model; 

	Controller(Player player1, Player player2, Model model) {
		this.player1 = player1;
		this.player2 = player2;
		
		this.model = model;
		
		this.player1Token = this.player1.initialise(this, 1);
		this.player2Token = this.player2.initialise(this, 2);
	}
	
	
	/**
	 * Allows a Player to make a move by specifying the column they want to place a counter in.
	 * @param column
	 * 		An integer starting from 0 representing a column
	 * @param identifier
	 * 		A token used to identify the Player object making the call
	 * @return
	 * 		True if the move was accepted, false if the move was invalid or it is not the player's turn.
	 */
	public synchronized boolean move(int column, String identifier){
		int activePlayer = model.getActivePlayer();

		//the move will be rejected if the identifier doesn't correspond to the currently active player
		if( !( (identifier.equals(player1Token) && activePlayer == 1) || 
				(identifier.equals(player2Token) && activePlayer == 2) ) ) {
			return false;
		}
		
		//Attempt to update the game state
		if(model.placeCounter(column)) {
			return true;
		}

		return false;
	}

}
