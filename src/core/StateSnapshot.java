package core;

/**
 * This class represents an immutable snapshot of the game state at a point in time. It 
 * is simply a vessel for moving game state data as a single entity. It does no validation 
 * of it's own and relies on the data it is given being accurate.
 */
public class StateSnapshot{
	public final int[][] grid;
	public final int activePlayer;
	public final int[] stackHeights;
	public final int winner;
	public final boolean deadlocked;
	
	public StateSnapshot(int activePlayer, int[][] grid, int[] stackHeights, int winner, boolean deadlocked){
		this.activePlayer = activePlayer;
		this.grid = grid;
		this.stackHeights = stackHeights;
		this.winner = winner;
		this.deadlocked = deadlocked;
	}
}
