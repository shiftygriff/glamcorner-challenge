package core;

import java.util.Observable;

/**
 * This class track the game state. Player objects can only modify it via a Controller object.
 * It implements the Observable interface and notifies it's Observers whenever it's state has changed.
 *
 */
public class Model extends Observable{
	public static final int emptyCell = 0;
	
	//dimensions of the grid
	public final int width;
	public final int height;
	
	private int[][] grid; //represents the game board. Cells contain numbers corresponding to players, with a 0 representing an empty cell.
	private int activePlayer; //tracks which player's turn it is
	private int[] stackHeights; //provides a way to quickly look up how many counters are in each column
	private int winner; //will contain the player number of the winner when a winning state is reached, otherwise it will contain 0
	private boolean deadlocked; //a flag that indicates when the game is tied
	
	public Model(int width, int height){
		this.width = width;
		this.height = height;
		this.grid = new int[height][width];
		//populate the grid with empty cells
		for(int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				this.grid[i][j] = Model.emptyCell;
			}
		}
		
		//set all stack heights to 0
		this.stackHeights = new int[width];		
		for(int i = 0; i < width; i++) {
			stackHeights[i] = 0;
		}
		this.activePlayer = 1;
		this.winner = 0;
		this.deadlocked = false;
	}
	
	/**
	 * @return
	 * 		The player whose turn it is.
	 */
	synchronized int getActivePlayer() {
		return activePlayer;
	}
	
	/**
	 * returns true only if a column number represents a valid move
	 * @param column
	 * 		The column to place a counter in.
	 * @return
	 * 		True if the game is still in progress and the move is valid, otherwise false.
	 */
	private boolean isValidColumn(int column) {
		if(winner != 0) {
			return false;
		}
		
		if(column < 0 || column >= width) {
			return false;
		}
		
		if(stackHeights[column] == height) {
			return false;
		}
		return true;
	}
	
 
	/**
	 * Places a counter for the currently active player, then toggles the active 
	 * player and checks for win or draw conditions. This method is the only way 
	 * to update the game state and will notify all Observers that the state has changed.
	 * @param column
	 * 		The column to place a counter in.
	 * @return
	 * 		True if the move is accepted, or false if it is rejected
	 */
	synchronized boolean placeCounter(int column){
		if(!isValidColumn(column)) {
			return false;
		}
		
		int currentPlayer = activePlayer;
		
		int row = height - 1 - stackHeights[column];
		
		if(currentPlayer == 1) {
			grid[row][column] = 1;
			activePlayer = 2;
		}
		else if(currentPlayer == 2) {
			grid[row][column] = 2;
			activePlayer = 1;
		}
		else {
			throw new RuntimeException("Game is in invalid state. This indicates a bug.");
		}
		
		stackHeights[column] ++;
		
		//check to see if the move has won the game
		if(checkVictory(row, column)) {
			winner = currentPlayer;
		}
		
		if(winner == 0) {
			//check to see if the move has deadlocked the game
			boolean columnsFull = true;
			for(int i = 0; i < width; i++) {
				if(stackHeights[i] != height) {
					columnsFull = false;
					break;
				}
			}
			if(columnsFull) {
				deadlocked = true;
			}
		}
		
		setChanged();
		notifyObservers();
		return true;
	}
	
	/**
	 * Takes a set of coordinates and checks if the counter at that position is part of a line of four
	 * @param row
	 * 		The row the counter to check is in.
	 * @param column
	 * 		The column the counter to check is in.
	 * @return
	 * 		True if the counter at the position is part of a winning line of four, false 
	 * 		if it isn't or if the position contains no counter
	 */
	private boolean checkVictory(int row, int column) {
		int targetPlayer = grid[row][column];
		if(targetPlayer != 1 && targetPlayer != 2) {
			return false;
		}
		
		//check for a vertical victory
		int adjacentCounters = 1;
		for(int i = 1; i < 4; i++) {
			if(row + i >= height) {
				break;
			}
			if(grid[row + i][column] == targetPlayer){
				adjacentCounters ++;
				continue;
			}
			break;
		}
		for(int i = 1; i < 4; i++) {
			if(row - i < 0) {
				break;
			}
			if(grid[row - i][column] == targetPlayer){
				adjacentCounters ++;
				continue;
			}
			break;
		}
		if(adjacentCounters >= 4) {
			return true;
		}
		
		//check for a horizontal victory
		adjacentCounters = 1;
		for(int i = 1; i < 4; i++) {
			if(column + i >= width) {
				break;
			}
			if(grid[row][column + i] == targetPlayer){
				adjacentCounters ++;
				continue;
			}
			break;
		}
		for(int i = 1; i < 4; i++) {
			if(column - i < 0) {
				break;
			}
			if(grid[row][column - i] == targetPlayer){
				adjacentCounters ++;
				continue;
			}
			break;
		}
		if(adjacentCounters >= 4) {
			return true;
		}
		
		//check for a diagonal victory (top-left to bottom-right)
		adjacentCounters = 1;
		for(int i = 1; i < 4; i++) {
			if(column + i >= width || row + i >= height) {
				break;
			}
			if(grid[row + i][column + i] == targetPlayer){
				adjacentCounters ++;
				continue;
			}
			break;
		}
		for(int i = 1; i < 4; i++) {
			if(column - i < 0 || row - i < 0) {
				break;
			}
			if(grid[row - i][column - i] == targetPlayer){
				adjacentCounters ++;
				continue;
			}
			break;
		}
		if(adjacentCounters >= 4) {
			return true;
		}
		
		//check for a diagonal victory (bottom-left to top-right)
		adjacentCounters = 1;
		for(int i = 1; i < 4; i++) {
			if(column + i >= width || row - i < 0) {
				break;
			}
			if(grid[row - i][column + i] == targetPlayer){
				adjacentCounters ++;
				continue;
			}
			break;
		}
		for(int i = 1; i < 4; i++) {
			if(column - i < 0 || row + i >= height) {
				break;
			}
			if(grid[row + i][column - i] == targetPlayer){
				adjacentCounters ++;
				continue;
			}
			break;
		}
		if(adjacentCounters >= 4) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Allows the Player objects to retrieve the current state once they have been notified of a change.
	 * @return
	 * 		A copy of the game state, encapsulated in an immutable object.
	 */
	public synchronized StateSnapshot getState() {
		return new StateSnapshot(activePlayer, grid, stackHeights, winner, deadlocked);
	}
	
}
