package core;

import player.ComputerPlayer;
import player.HumanPlayer;
import player.Player;

/**
 * This class is the entry point for the program. 
 */
public class GameManager {
	
	/**
	 * Sets up the components required by the MVC pattern and starts threads running. 
	 * This method will only terminate when all Player threads have terminated (i.e. at 
	 * the end of the game).
	 * @param args
	 */
	public static void main(String args[]) {
		Model model = new Model(7,6);
		Player human = new HumanPlayer(model);
		Player computer = new ComputerPlayer(model);
		Controller controller = new Controller(human, computer, model);
		
		Thread humanView = new Thread(human);
		Thread computerView = new Thread(computer);
		humanView.start();
		computerView.start();
		
		try {
			humanView.join(0);
			computerView.join(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
