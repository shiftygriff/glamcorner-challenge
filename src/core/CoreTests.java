package core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import player.ComputerPlayer;
import player.HumanPlayer;
import player.Player;

/**
 * Provides unit tests for the core package.
 *
 */
public class CoreTests {
	
	/**
	 * Validate that the Model tracks whose turn it is
	 */
	@Test
	public void testTurnTracking(){
		Model model = new Model(7,6);
		StateSnapshot state = model.getState();
		assertEquals(1, state.activePlayer);
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(2, state.activePlayer);
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(1, state.activePlayer);
	}
	
	/**
	 * Validate that the Model positions counters correctly, including stacking
	 */
	@Test
	public void testCounterPositioning(){
		Model model = new Model(7,6);
		model.placeCounter(3);
		StateSnapshot state = model.getState();
		assertEquals(1, state.grid[5][3]);
		assertEquals(1, state.stackHeights[3]);
		
		model.placeCounter(5);
		state = model.getState();
		assertEquals(1, state.grid[5][3]);
		assertEquals(2, state.grid[5][5]);
		assertEquals(1, state.stackHeights[3]);
		assertEquals(1, state.stackHeights[5]);

		
		model.placeCounter(5);
		state = model.getState();
		assertEquals(1, state.grid[5][3]);
		assertEquals(2, state.grid[5][5]);
		assertEquals(1, state.grid[4][5]);
		assertEquals(1, state.stackHeights[3]);
		assertEquals(2, state.stackHeights[5]);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(1, state.grid[5][3]);
		assertEquals(2, state.grid[5][5]);
		assertEquals(1, state.grid[4][5]);
		assertEquals(2, state.grid[4][3]);
		assertEquals(2, state.stackHeights[3]);
		assertEquals(2, state.stackHeights[5]);
	}
	
	/**
	 * Validate that the Model detects a draw
	 */
	@Test
	public void testDrawCondtion(){
		Model model = new Model(3,3);
		StateSnapshot state = model.getState();
		assertEquals(false, state.deadlocked);
		assertEquals(0, state.winner);

		//fill the grid with counters
		//the grid size is deliberately reduced to prevent a win condition from being triggered
		//a draw should only be detected after the last counter is placed
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				model.placeCounter(j);
				state = model.getState();
				if(i < 2 || j < 2) {
					assertEquals(false, state.deadlocked);
				}
				else {
					assertEquals(true, state.deadlocked);
				}
				assertEquals(0, state.winner);
			}
		}
	}
	
	/**
	 * Validate that the Model detects a horizontal win
	 */
	@Test
	public void testHorizontalWinCondition(){
		Model model = new Model(7,6);
		StateSnapshot state = model.getState();
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(6);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(1);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(2);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(6);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(1, state.winner);
	}
	
	/**
	 * Validate that the Model detects a vertical win
	 */
	@Test
	public void testVerticalWinCondition(){
		Model model = new Model(7,6);
		StateSnapshot state = model.getState();
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(6);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(6);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(1, state.winner);
	}
	
	/**
	 * Validate that the Model detects a diagonal (from bottom left) win
	 */
	@Test
	public void testLeftDiagonalWinCondition(){
		Model model = new Model(7,6);
		StateSnapshot state = model.getState();
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(1);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(1);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(2);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(2);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(2);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(6);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(1, state.winner);		
	}




	/**
	 * Validate that the Model detects a diagonal (from bottom right) win
	 */
	@Test
	public void testRightDiagonalWinCondition(){
		Model model = new Model(7,6);
		StateSnapshot state = model.getState();
		
		model.placeCounter(6);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(5);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(5);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(4);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(4);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(4);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(0);
		state = model.getState();
		assertEquals(0, state.winner);
		
		model.placeCounter(3);
		state = model.getState();
		assertEquals(1, state.winner);	
	}
	
	/**
	 * Validate that the Model rejects moves if the game is over
	 */
	@Test
	public void testCheckGameOver(){
		Model model = new Model(7,6);
		//play a game to victory
		model.placeCounter(0);
		model.placeCounter(6);
		model.placeCounter(0);
		model.placeCounter(3);
		model.placeCounter(0);
		model.placeCounter(6);
		model.placeCounter(0);
		assertFalse(model.placeCounter(3));
		
		model = new Model(2,2);
		//play a game to a draw
		model.placeCounter(0);
		model.placeCounter(1);
		model.placeCounter(0);
		model.placeCounter(1);
		assertFalse(model.placeCounter(0));
	}
	
	/**
	 * Validate that the Model rejects moves if the column is full
	 */
	@Test
	public void testCheckColumnFull(){
		Model model = new Model(3,3);
		model.placeCounter(0);
		model.placeCounter(0);
		model.placeCounter(0);
		assertFalse(model.placeCounter(0));
	}
	
	/**
	 * Validate that the Model rejects moves if the selected column doesn't exist
	 */
	@Test
	public void testCheckColumnExists(){
		Model model = new Model(3,3);
		assertFalse(model.placeCounter(5));
	}
	
	/**
	 * Validate that the Controller rejects moves without a valid authentication token
	 */
	@Test
	public void testAuthentication(){
		Model model = new Model(7,6);
		Player human = new HumanPlayer(model);
		Player computer = new ComputerPlayer(model);
		Controller controller = new Controller(human, computer, model);
		assertFalse(controller.move(0, "some token"));
	}

}