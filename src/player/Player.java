package player;

import java.util.UUID;
import java.util.Observable;
import java.util.Observer;
import java.lang.Runnable;
import core.Model;
import core.Controller;

/**
 * Takes care of the logic and data common to all player implementations.
 */
public abstract class Player implements Observer, Runnable{
	
	private String token; // a unique secret token that allows the Player to authenticate with the Controller
	Controller controller;
	Model model;
	private boolean initialised = false;
	int playerNumber; // a unique number that this Player can use to identify itself in the StateSnapshots it receives
	boolean newStateAvailable = true; //a flag that indicates whether there is new state data available on the Model
	
	
	Player(Model model){
		this.token = UUID.randomUUID().toString(); //generate a unique token
		this.model = model;
		model.addObserver(this); //set up the Player to receive notifications whenever the Model changes it's state
	}
	
	/**
	 * Lets the Model notify the Player whenever it changes
	 */
	@Override
	public void update(Observable state, Object arg) {
		this.newStateAvailable = true;
	}
	
	/**
	 * A method intended to be called by the Controller when it is ready to begin the game. 
	 * It can only be called once and is the only way to access this Player's identifying token. 
	 * Since a Controller only sets it's tokens by calling this method, if any malicious 
	 * class tries to use this method to steal a token, the Controller won't be able to 
	 * identify it anyway and it will only succeed in breaking the game.
	 * @param controller
	 * 		The Controller that this Player will send their moves to.
	 * @return
	 * 		The unique token used to identify this player.
	 */
	public String initialise(Controller controller, int playerNumber) {
		if(initialised) {
			throw new RuntimeException("Player has already been initialised");
		}
		this.controller = controller;
		this.playerNumber = playerNumber;
		this.initialised = true;
		return this.token;
	}
	
	/**
	 * A wrapper for the Controller.move() method that takes care of identifying the player 
	 * to the controller.
	 * @param column
	 * 		The column to drop a counter in
	 * @return
	 * 		True if the move was accepted, false if the move was invalid or it is not 
	 * 		this player's turn.
	 */
	boolean placeMove(int column) {
		return controller.move(column, this.token);
	}
	
}
