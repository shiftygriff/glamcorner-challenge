package player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import core.Model;
import core.StateSnapshot;

/**
 * Implementation of a Player that can be controlled by a human user.
 */
public class HumanPlayer extends Player {

	public HumanPlayer(Model model){
		super(model);
	}
	
	/**
	 * To be run as a Thread. Continuously checks for updates from the Model and 
	 * displays them to the user. Takes user input and relays it to the Controller. 
	 * This method will not terminate until the game is over.
	 */
	public void run() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Beginning a new game");
		while(true) {
			if(newStateAvailable) {
				StateSnapshot state = this.model.getState();
				newStateAvailable = false;
				
				System.out.println();
				// display the board
				System.out.println("--------------------------------------------------");
				for(int i = 0; i < model.height; i++) {
					for(int j = 0; j < model.width; j++) {
						if(state.grid[i][j] == playerNumber) {
							System.out.print("X ");
						}
						else if(state.grid[i][j] == Model.emptyCell) {
							System.out.print(". ");
						}
						else {
							System.out.print("O ");
						}
					}
					System.out.println();
					System.out.println();
				}
				
				//check for a winner
				if(state.winner == playerNumber) {
					System.out.println("You won the game!");
					break;
				}
				else if(state.winner != 0) {
					System.out.println("You lose! Better luck next time.");
					break;
				}
				
				//check for a deadlock
				if(state.deadlocked) {
					System.out.println("The game is a draw!");
					break;
				}
				
				if(state.activePlayer == playerNumber) {
					System.out.println("It is your turn. Select a column to drop a counter into by entering a number from the options below.");
					
					for(int i = 0; i < model.width; i++) {
						if(state.stackHeights[i] < model.height) {
							int columnNumber = i + 1;
							System.out.println("[" + columnNumber + "] - Column " + columnNumber);
						}
					}
					System.out.println("Type a column number and press enter");
					int selectedColumn;
					while(true) {
				        String input;
						try {
							input = br.readLine();
						} catch (IOException e1) {
							System.out.println("Something went wrong. Please try again.");
							continue;
						}
						try {
							selectedColumn = Integer.parseInt(input) - 1;
						}
						catch(NumberFormatException e2) {
							System.out.println("Invalid selection. Please enter a number from the options above.");
							continue;
						}
						if(placeMove(selectedColumn)) {
							break;
						}
						System.out.println("Invalid selection. Please enter a number from the options above.");
					}
				
				}
				else {
					System.out.print("Waiting for the other player to move");
				}
			}
			else {
				System.out.print(".");
			}
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
