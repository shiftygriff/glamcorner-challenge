package player;

import java.util.ArrayList;

import core.Model;
import core.StateSnapshot;

/**
 * Implementation of a Player that is controlled by a computer.
 */
public class ComputerPlayer extends Player {

	public ComputerPlayer(Model model){
		super(model);
	}
	
	/**
	 * To be run as a Thread. Continuously checks for updates from the Model and 
	 * places moves with the Controller. The computer is dumb and only plays by 
	 * randomly selecting a valid move each turn. This method will not terminate 
	 * until the game is over.
	 */
	public void run() {
		while(true) {
			if(newStateAvailable) {
				StateSnapshot state = this.model.getState();
				newStateAvailable = false;
				
				//stop the thread if the game is over
				if(state.winner != 0) {
					break;
				}
				
				//stop the thread if there is a deadlock
				if(state.deadlocked) {
					break;
				}
				
				if(state.activePlayer == playerNumber) {
					//artificial delay to simulate a human player
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					//compile a list of valid moves
					ArrayList<Integer> validMoves = new ArrayList<Integer>();
					for(int i = 0; i < model.width; i++) {
						if(state.stackHeights[i] < model.height) {
							validMoves.add(i);
						}
					}
					
					//pick a move from the list at random
					int moveIndex = (int)Math.floor(Math.random() * validMoves.size());
					if(!placeMove(validMoves.get(moveIndex))) {
						throw new RuntimeException("Computer player made an invalid move. This indicates a bug.");
					}
				}
			}
			
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
